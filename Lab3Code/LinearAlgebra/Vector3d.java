package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public static void main(String[] args) {
        Vector3d myVec = new Vector3d(1,2,3);
        System.out.println(myVec.dotProduct(myVec));
    }

    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double magnitude()
    {
        double magnitude = Math.sqrt(this.x*this.x + this.y*this.y + this.z+this.z);
        return magnitude;
    }

    public double dotProduct(Vector3d sVec)
    {
        double product = this.x * sVec.x + this.y * sVec.y + this.z * sVec.z;
        return product;
    }

    public Vector3d add(Vector3d sVec)
    {
        Vector3d newVec = new Vector3d(this.x + sVec.x, this.y + sVec.y, this.z + sVec.z);
        return newVec;
    }

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }
}
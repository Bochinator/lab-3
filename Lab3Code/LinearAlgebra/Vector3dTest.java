package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTest {
    @Test
    public void testGets()
    {
        Vector3d myVec = new Vector3d(1,2,3);
        assertEquals(1, myVec.getX());
        assertEquals(2, myVec.getY());
        assertEquals(3, myVec.getZ());
    }

    @Test
    public void testMagnitude()
    {
        Vector3d myVec = new Vector3d(1,2,3);
        assertEquals(3.3166247903554, myVec.magnitude());
    }

    @Test
    public void testDot()
    {
        Vector3d myVec = new Vector3d(1,2,3);
        Vector3d secondVec = new Vector3d(2,3,4);
        assertEquals(20, myVec.dotProduct(secondVec));
    }
    
    @Test
    public void testAdd()
    {
        Vector3d myVec = new Vector3d(1,2,3);
        Vector3d secondVec = new Vector3d(2,3,4);
        Vector3d resultVec = myVec.add(secondVec);
        assertEquals(3, resultVec.getX());
        assertEquals(5, resultVec.getY());
        assertEquals(7, resultVec.getZ());
    }
}
